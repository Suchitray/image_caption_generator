# Image Caption Generator #

### Contents ###

* Introduction
* Prerequisites
* Data preparation
* Loading the training set
* Extracting features from the images
* Tokenizing the vocabulary 
* Preparing the data using generator function
* Creating the CNN-RNN model
* Training and testing the model
* Conclusion
* References

### Introduction ###

Image caption generation is an artifcial intelligence problem of generating a caption for any given image.

It requires the knowledge of both computer vision to understand the image and natural language processing to turn the information from the image to words in a sequence.

### Prerequisites ###

For this project, one must have basic knowledge about the following topics:

* Deep learning concepts such as transfer learning, backpropagation, Convolution Neural Networks, Recurrent Neural Networks, gradient descent and Keras library.
* Python syntax and text processing in python.

The required libraries are:

* keras
* tensorflow
* numpy
* tqdm
* matplotlib

For the image caption generator, I have used the __Flicker8k_Dataset__ which can be downloaded using the link given below: 

[Flicker8k_Dataset](https://github.com/jbrownlee/Datasets/releases/download/Flickr8k/Flickr8k_Dataset.zip)

This dataset consists a total of 8000 images, 6000 for the training set, 1000 for the dev set and 1000 for the test set.
Each image consists of 5 captions associated with it.

### Data preparation ###

Firstly we prepare our data for further usage. One of the files included in this repository is __“Flickr8k.token.txt”__ which contains the name of each image along with its 5 captions.
We read this file, and create a dictionary __"descriptions"__, that contains the image name as the key and the captions as the value. 
Then we go onto the process of text cleaning, which includes:

* Converting the words to lower case.
* Removing the punctuations from each word.
* Removing hanging 's' and 'a'.
* Removing words with numbers in them.

### Loading the training set ###

The text file __“Flickr_8k.trainImages.txt”__ contains the names of the images that belong to the training set. 
We load these images into the list __"trainImages"__.
Now, we load the descriptions of these images from the dictionary __“descriptions”__ into the Python dictionary __“train_descriptions”__.
While loading these images into __“train_descriptions”__, we add a start sequence token __"startseq"__ and a end sequence token __"endseq"__ to every caption.

### Extracting features from the images ###

For extracting features from the images we use the concept of transfer learning, in which we use the pre-trained model that has been already trained on large datasets and extract the features from these models and use them for our tasks.
For this project we used the Xception model, which was trained on the imagenet dataset. We remove the final fully connected layer and obtain a 2048 feature vector which can be used as an input for our neural network.

The function __"extract_features()"__ will extract features for all images and we will map image names with their respective feature array. We have stored the extracted features in the file __"features.p"__ contained in this repository.

### Tokenizing the vocabulary  ###

We tokenize each word so that we map each word in our vocabulary to a unique number. 
Keras library provides us with the tokenizer function that we will use to create tokens from our vocabulary.
The function __"create_tokenizer"__ is used to implement this.

### Preparing the data using generator function ###

We must understand that for generating a caption, it is not only the image but also a partial caption that helps to predict the next word in the sequence.
Since we are processing sequences, we will employ a Recurrent Neural Network to read these partial captions.

We have to train our model on 6000 images in which each image contains a 2048 length feature vector and the caption is also represented as numbers. This amount of data for 6000 images is a large amount of memory, so we used the generator method that will yield batches.
A generator function is like an iterator which resumes the functionality from the point it left the last time it was called.
### Creating the CNN-RNN model ###

Since our model consists of two inputs, we will be using the keras Model from the functional API which allows us to create merge models. Our model consists of three main parts:

* __Image feature extractor model__:

	* Our input feature is of shape(2048,0). With the help of a dense layer we will reduce the dimensions to 256 nodes.
	
* __Partial caption sequence model__:

	* We include the embedding layer to handle the textual input which is then followed by a LSTM layer. LSTM stands for Long short term memory, it is a type of RNN (recurrent neural network) which is well suited for sequence prediction problems.

* __Decoder model__:

	* We merge the outputs of the above two layers, followed by a dense layer to give our output. The final layer consists of the number of nodes equal to our vocabulary size.

The view of the model is included in this repository's files.

### Training and testing the model ###

6000 images were used to train the model by generating the input and output sequences in batches and fitting them to the model using model.fit_generator() method.

The __"Models"__ folder in the repository contains the saved models.

We load our test images and generate captions using the saved model.

### Conclusion ###

For this project I have used the __Flicker8k_Dataset__. There are bigger datasets like the __Flickr_30K__ and the __MSCOCO__ which take weeks to train.
A bigger dataset will result in a better model, and will generate more suitable captions.

### References ###

* [https://towardsdatascience.com/image-captioning-with-keras-teaching-computers-to-describe-pictures-c88a46a311b8](https://towardsdatascience.com/image-captioning-with-keras-teaching-computers-to-describe-pictures-c88a46a311b8)
* [https://www.youtube.com/watch?v=yk6XDFm3J2c](https://www.youtube.com/watch?v=yk6XDFm3J2c)
* [https://machinelearningmastery.com/develop-a-deep-learning-caption-generation-model-in-python/](https://machinelearningmastery.com/develop-a-deep-learning-caption-generation-model-in-python/)